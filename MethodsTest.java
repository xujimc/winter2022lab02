public class MethodsTest{
	public static void main(String[] args){
		int x = 10;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		methodTwoInputNoReturn(x,3.5);
		int z = methodNoInputReturnInt();
		System.out.println(z);
		double y = sumSquareRoot(6,3);
		System.out.println(y);
		String s1="hello", s2="goodbye";
		System.out.println("the length of s1 is "+s1.length());
		System.out.println("the length of s2 is "+s2.length());
		System.out.println(SecondClass.addOne(50));
		SecondClass sc= new SecondClass();
		System.out.println(sc.addTwo(50));
		System.out.println();
	}
	public static void methodNoInputNoReturn(){
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x =50;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int x){
		System.out.println("Inside the method one input no return");
		System.out.println(x);
	}
	public static void methodTwoInputNoReturn(int x, double y){
		
	}
	public static int methodNoInputReturnInt(){
		return 6;
	}
	public static double sumSquareRoot(int x, int y){
		int temp = x+y;
		return Math.sqrt(temp);
	}
}