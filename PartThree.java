public class PartThree{
	public static void main(String[] args){
		System.out.println("enter length of the square");
		double squareLength=new java.util.Scanner(System.in).nextDouble();
		System.out.println("square area: "+AreaComputations.areaSquare(squareLength));
		System.out.println("enter length of the rectangle");
		double rectangleLength=new java.util.Scanner(System.in).nextDouble();
		System.out.println("enter width of the rectangle");
		double rectangleWidth=new java.util.Scanner(System.in).nextDouble();
		System.out.println("rectangle area: "+new AreaComputations().areaRectangle(rectangleLength, rectangleWidth));
	}
}